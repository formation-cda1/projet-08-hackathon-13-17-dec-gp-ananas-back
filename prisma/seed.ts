import { PrismaClient } from "@prisma/client"

const { players } = require('./seeds/players')
const { gameModes } = require('./seeds/gameModes')
const { categories } = require('./seeds/categories')

const prisma = new PrismaClient();

async function main() {
  console.log('Seeding game modes...')

  console.log('Seeding categories...')
  for (const c of categories) await prisma.category.create({ data: c })

  console.log('Seeding game modes...')
  for (const gm of gameModes) await prisma.gameMode.create({ data: gm })

  console.log('Seeding players...')
  for (const p of players) await prisma.player.create({ data: p })

  console.log('Seeding finished.')
}

main().catch((e) => {
    console.error(e)
    process.exit(1)
}).finally(async () => {
    await prisma.$disconnect()
})