import { Prisma } from "@prisma/client"

export const categories: Prisma.CategoryCreateInput[] = [
  {
    name: "Front-end",
    questions: {
      create: [
        {
          content: "Quel formulaire de saisie HTML utiliseriez-vous si vous vouliez que l'utilisateur fournisse une valeur de chaîne courte non prédéfinie ?",
          responses: {
            create: [
              {
                content: `<input type="text">`,
                trueOrFalse: true,
              },
              {
                content: `<input type="string">`,
                trueOrFalse: false,
              },
              {
                content: `<input type="shortString">`,
                trueOrFalse: false,
              },
              {
                content: `<input type="notDefined">`,
                trueOrFalse: false,
              }
            ]
          }

        },
        {
          content: "Quelle propriété CSS permet de gérer l'affichage des éléments dépassant de leur conteneur ?",
          responses: {
            create: [
              {
                content: `object-fit`,
                trueOrFalse: false,
              },
              {
                content: `outline-style`,
                trueOrFalse: false,
              },
              {
                content: `overflow`,
                trueOrFalse: true,
              },
              {
                content: `placeholder`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Parmi ces 4 langages, lequel n'est-il pas basé sur du Javascript ?",
          responses: {
            create: [
              {
                content: `Node.JS`,
                trueOrFalse: false,
              },
              {
                content: `Deno`,
                trueOrFalse: false,
              },
              {
                content: `Java`,
                trueOrFalse: true,
              },
              {
                content: `Typescript`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "L'élément svg est utilisé pour :",
          responses: {
            create: [
              {
                content: `Dessiner des graphiques via des scripts`,
                trueOrFalse: false,
              },
              {
                content: `Définir des graphiques vectoriels pour le Web`,
                trueOrFalse: true,
              },
              {
                content: `Définir des graphiques visuels pour le Web`,
                trueOrFalse: false,
              },
              {
                content: `Aucune des réponses ci-dessus`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Laquelle des réponses suivantes est une méthode de formulaire valide ?",
          responses: {
            create: [
              {
                content: `Send et Get`,
                trueOrFalse: false,
              },
              {
                content: `Post et Get`,
                trueOrFalse: true,
              },
              {
                content: `Push et Pull`,
                trueOrFalse: false,
              },
              {
                content: `Post et Pull`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Que signifie l'acronyme CSS ?",
          responses: {
            create: [
              {
                content: `Cute Stylish Sheeps`,
                trueOrFalse: false,
              },
              {
                content: `Cascading Style Sheet`,
                trueOrFalse: true,
              },
              {
                content: `Cascading Swift Sheet`,
                trueOrFalse: false,
              },
              {
                content: `Cabbage Sweet Soup`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Lequel des paramètres suivants n'est pas un moyen de spécifier la couleur noire en CSS ?",
          responses: {
            create: [
              {
                content: `#000000`,
                trueOrFalse: false,
              },
              {
                content: `rgb(00, 00, 00)`,
                trueOrFalse: true,
              },
              {
                content: `black`,
                trueOrFalse: false,
              },
              {
                content: `#000`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quelle est la syntaxe correcte d'un dégradé linéaire ?",
          responses: {
            create: [
              {
                content: `background: linear-gradient(red, blue);`,
                trueOrFalse: true,
              },
              {
                content: `background: linear(red, blue);`,
                trueOrFalse: false,
              },
              {
                content: `background: radial-gradient(red, blue);`,
                trueOrFalse: false,
              },
              {
                content: `backgroud: gradient(red, blue);`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quel est le rôle d'un intercepteur ?",
          responses: {
            create: [
              {
                content: `Il exécute une variété de tâches (par exemple, définir les en-têtes sur chaque requête, mettre en cache les requêtes) implicitement sur chaque méthode http.`,
                trueOrFalse: true,
              },
              {
                content: `Il intercepte les actions de l'utilisateur afin de le rediriger vers une nouvelle page.`,
                trueOrFalse: false,
              },
              {
                content: `Il effectue des optimisations sur les appels HTTP.`,
                trueOrFalse: false,
              },
              {
                content: `Il est principalement utilisé pour effectuer des tests.`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Dans Angular, quel est le type de 'valueChanges' sur un FormControl ?",
          responses: {
            create: [
              {
                content: `Observable<any>`,
                trueOrFalse: true,
              },
              {
                content: `Any`,
                trueOrFalse: false,
              },
              {
                content: `Array<any>`,
                trueOrFalse: false,
              },
              {
                content: `Unknown`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Dans quel élément HTML peut-on insérer du JavaScript ?",
          responses: {
            create: [
              {
                content: `<js>`,
                trueOrFalse: false,
              },
              {
                content: `<javascript>`,
                trueOrFalse: false,
              },
              {
                content: `<script>`,
                trueOrFalse: true,
              },
              {
                content: `<scripting>`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quelle est la syntaxe correcte pour référencer le script externe 'mojito.js' ?",
          responses: {
            create: [
              {
                content: `<script name="mojito.js">`,
                trueOrFalse: false,
              },
              {
                content: `<script src="mojito.js">`,
                trueOrFalse: true,
              },
              {
                content: `<script href="mojito.js">`,
                trueOrFalse: false,
              },
              {
                content: `<script drink="mojito.js">`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Comment écrire une déclaration IF en Javascript ?",
          responses: {
            create: [
              {
                content: `if i = 5 then`,
                trueOrFalse: false,
              },
              {
                content: `if i == 5 then`,
                trueOrFalse: false,
              },
              {
                content: `if i = 5`,
                trueOrFalse: false,
              },
              {
                content: `if (i == 5)`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Que signigie l'acronyme HTML ?",
          responses: {
            create: [
              {
                content: `Home Tool Markup Language`,
                trueOrFalse: false,
              },
              {
                content: `Hyperlinks and Text Markup Language`,
                trueOrFalse: false,
              },
              {
                content: `Hyper Text Markup Language`,
                trueOrFalse: true,
              },
              {
                content: `Highly Toxic Magnesite Leaks`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quel est la syntaxe CSS correcte ?",
          responses: {
            create: [
              {
                content: `{body; color: black;}`,
                trueOrFalse: false,
              },
              {
                content: `{body:color=black;}`,
                trueOrFalse: false,
              },
              {
                content: `body:color=black;`,
                trueOrFalse: false,
              },
              {
                content: `body {color: black;}`,
                trueOrFalse: true,
              },
            ]
          }
        },
      ]
    }
  },
  {
    name: "Back-end",
    questions: {
      create: [
        {
          content: "Parmi ces propositions, laquelle est correcte ?",
          responses: {
            create: [
              {
                content: `Le backend d'une application utilise le frontend pour accéder aux données de la base de données.`,
                trueOrFalse: false,
              },
              {
                content: `Le backend d'une application est pris en charge par le frontend et le serveur via HTTP.`,
                trueOrFalse: false,
              },
              {
                content: `Le backend d'une application est la partie qui prend en charge et fait fonctionner le frontend.`,
                trueOrFalse: false,
              },
              {
                content: `Le frontend d'une application est indépendant du développement du backend.`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Que signifie l'acronyme SQL ?",
          responses: {
            create: [
              {
                content: `Structured Question Language`,
                trueOrFalse: false,
              },
              {
                content: `Strong Question Language`,
                trueOrFalse: false,
              },
              {
                content: `Structured Query Language`,
                trueOrFalse: true,
              },
              {
                content: `Super Quasar Landscape`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quelle requête SQL permet de mettre à jour les données dans une base de données ?",
          responses: {
            create: [
              {
                content: `MODIFY`,
                trueOrFalse: false,
              },
              {
                content: `SAVE AS`,
                trueOrFalse: false,
              },
              {
                content: `UPDATE`,
                trueOrFalse: true,
              },
              {
                content: `SAVE`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Dans SQL, comment sélectionner une colonne 'prenom' dans la table 'Utilisateurs' ?",
          responses: {
            create: [
              {
                content: `SELECT Utilisateurs.prenom`,
                trueOrFalse: false,
              },
              {
                content: `EXTRACT prenom FROM Utilisateurs`,
                trueOrFalse: false,
              },
              {
                content: `SELECT prenom FROM Utilisateurs`,
                trueOrFalse: true,
              },
              {
                content: `GET prenom FROM Utilisateurs`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Dans SQL, comment sélectionner toutes les colonnes dans la table 'Utilisateurs' ?",
          responses: {
            create: [
              {
                content: `SELECT * FROM Utilisateurs`,
                trueOrFalse: true,
              },
              {
                content: `SELECT [all] FROM Utilisateurs`,
                trueOrFalse: false,
              },
              {
                content: `SELECT Utilisateurs`,
                trueOrFalse: false,
              },
              {
                content: `SELECT *.Utilisateurs`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Que signifie l'acronyme 'PHP' ?",
          responses: {
            create: [
              {
                content: `Personal Hypertext Processor`,
                trueOrFalse: false,
              },
              {
                content: `PHP: Hypertext Preprocessor`,
                trueOrFalse: true,
              },
              {
                content: `Private Home Page`,
                trueOrFalse: false,
              },
              {
                content: `Powerful Hyperdrive Processor`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Comment écrivez vous 'Hello World' en PHP ?",
          responses: {
            create: [
              {
                content: `echo "Hello World";`,
                trueOrFalse: true,
              },
              {
                content: `Document.Write("Hello World");`,
                trueOrFalse: false,
              },
              {
                content: `print "Hello World";`,
                trueOrFalse: false,
              },
              {
                content: `"Hello World"`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Comment insérez vous des commentaires en Python ?",
          responses: {
            create: [
              {
                content: `/* Commentaire *`,
                trueOrFalse: false,
              },
              {
                content: `#Commentaire`,
                trueOrFalse: true,
              },
              {
                content: `//Commentaire`,
                trueOrFalse: false,
              },
              {
                content: `***xXxCommentairexXx***`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quelle est l'extension de fichier correcte pour les fichiers en Python ?",
          responses: {
            create: [
              {
                content: `.pt`,
                trueOrFalse: false,
              },
              {
                content: `.pyt`,
                trueOrFalse: false,
              },
              {
                content: `.pyth`,
                trueOrFalse: false,
              },
              {
                content: `.py`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Qu'est-ce qui définit MySQL ?",
          responses: {
            create: [
              {
                content: `Développé, distribué et supporté par Oracle Corporation`,
                trueOrFalse: false,
              },
              {
                content: `Multi-plateforme, open-source et gratuit`,
                trueOrFalse: false,
              },
              {
                content: `Un système de gestion de base de données relationnelle`,
                trueOrFalse: false,
              },
              {
                content: `Toutes les options ci-dessus`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Quelle méthode en langage Python permet de supprimer l'espace au début et à la fin d'une chaîne de caractères ?",
          responses: {
            create: [
              {
                content: `trim()`,
                trueOrFalse: true,
              },
              {
                content: `ptrim()`,
                trueOrFalse: false,
              },
              {
                content: `strip()`,
                trueOrFalse: false,
              },
              {
                content: `len()`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quel opérateur permet de comparer deux valeurs ?",
          responses: {
            create: [
              {
                content: `<>`,
                trueOrFalse: false,
              },
              {
                content: `><`,
                trueOrFalse: false,
              },
              {
                content: `=`,
                trueOrFalse: false,
              },
              {
                content: `==`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Quelle déclaration SQL est utilisée pour retourner seulement les valeurs différentes ?",
          responses: {
            create: [
              {
                content: `SELECT UNIQUE`,
                trueOrFalse: false,
              },
              {
                content: `SELECT DISTINCT`,
                trueOrFalse: true,
              },
              {
                content: `SELECT DIFFERENT`,
                trueOrFalse: false,
              },
              {
                content: `SELECT NOT EQUAL`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quel mot-clé SQL est utilisé pour trier les résultats ?",
          responses: {
            create: [
              {
                content: `SORT BY`,
                trueOrFalse: false,
              },
              {
                content: `ORDER BY`,
                trueOrFalse: true,
              },
              {
                content: `ORDER`,
                trueOrFalse: false,
              },
              {
                content: `SORT`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quelle est la syntaxe correcte pour afficher 'Hello World' en Java ?",
          responses: {
            create: [
              {
                content: `print ("Hello World");`,
                trueOrFalse: false,
              },
              {
                content: `echo("Hellow World");`,
                trueOrFalse: false,
              },
              {
                content: `System.out.printInt("Hello World");`,
                trueOrFalse: true,
              },
              {
                content: `Console.WriteLine("Hello World");`,
                trueOrFalse: false,
              },
            ]
          }
        },
      ]
    }
  },
  {
    name: "DevOps",
    questions: {
      create: [
        {
          content: "Parmi les éléments suivants, lequel n'est pas un outil d'orchestration de conteneurs ?",
          responses: {
            create: [
              {
                content: `Kubernetes`,
                trueOrFalse: false,
              },
              {
                content: `Swarm`,
                trueOrFalse: false,
              },
              {
                content: `Docker Machine`,
                trueOrFalse: false,
              },
              {
                content: `Amazon ECS`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Quel objet de Kubernetes est utilisé pour stocker des données sensibles dans des Pods ?",
          responses: {
            create: [
              {
                content: `ConfigMaps`,
                trueOrFalse: false,
              },
              {
                content: `Secrets`,
                trueOrFalse: true,
              },
              {
                content: `Ingress`,
                trueOrFalse: false,
              },
              {
                content: `StatefullSets`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Qu'est-ce que Minikube ?",
          responses: {
            create: [
              {
                content: `Un cluster à noeud (node) unique pour le développement.`,
                trueOrFalse: true,
              },
              {
                content: `Un cluster multi-noeuds pour la production.`,
                trueOrFalse: false,
              },
              {
                content: `Une autre dénomination pour Kubernetes.`,
                trueOrFalse: false,
              },
              {
                content: `Un système d'exploitation.`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quels sont les composants des noeuds (nodes) de 'worker' ?",
          responses: {
            create: [
              {
                content: `Kubelet`,
                trueOrFalse: true,
              },
              {
                content: `Kube-proxy`,
                trueOrFalse: false,
              },
              {
                content: `Container Runtime`,
                trueOrFalse: false,
              },
              {
                content: `LoadBalancer (répartiteur de charge)`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Qu'est-ce que Jenkins ?",
          responses: {
            create: [
              {
                content: `Un ordonnanceur`,
                trueOrFalse: true,
              },
              {
                content: `Un outil de gestion de conteneurs`,
                trueOrFalse: false,
              },
              {
                content: `L'outil CLI de Rancher`,
                trueOrFalse: false,
              },
              {
                content: `Leeroy Jenkins, un paladin déterminé`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Qu'est-ce que Kubectl ?",
          responses: {
            create: [
              {
                content: `Un outil de ligne de commande pour le contrôle des clusters Kubernetes`,
                trueOrFalse: true,
              },
              {
                content: `Un outil de provisionnement des clusters Kubernetes`,
                trueOrFalse: false,
              },
              {
                content: `Il est utilisé pour contrôler les clusters Docker Swarm`,
                trueOrFalse: false,
              },
              {
                content: `Un outil de pour le provisionnement de Minikube`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Qu'est-ce que GIT ?",
          responses: {
            create: [
              {
                content: `Un langage de programmation`,
                trueOrFalse: false,
              },
              {
                content: `Un système de contrôle de versions`,
                trueOrFalse: true,
              },
              {
                content: `Une plateforme de dépôt à distance`,
                trueOrFalse: false,
              },
              {
                content: `le surnom de GitHub`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quelle est la commande pour obtenir le status en cours du dépôt Git ?",
          responses: {
            create: [
              {
                content: `-- status`,
                trueOrFalse: false,
              },
              {
                content: `git status`,
                trueOrFalse: true,
              },
              {
                content: `git config --status`,
                trueOrFalse: false,
              },
              {
                content: `git getStatus`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quelle est la commande pour initialiser Git sur le répertoire en cours d'utilisation ?",
          responses: {
            create: [
              {
                content: `initialize git`,
                trueOrFalse: false,
              },
              {
                content: `git start`,
                trueOrFalse: false,
              },
              {
                content: `start git`,
                trueOrFalse: false,
              },
              {
                content: `git init`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Quelle est la commande Git pour commit avec le message 'new email'",
          responses: {
            create: [
              {
                content: `git commit -m "New email"`,
                trueOrFalse: true,
              },
              {
                content: `git commit message "New email"`,
                trueOrFalse: false,
              },
              {
                content: `git commit -mess "New email"`,
                trueOrFalse: false,
              },
              {
                content: `git commit -log "New Email"`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Quels problèmes DevOps permet-il de résoudre ?",
          responses: {
            create: [
              {
                content: `Les cycles de déploiement longs`,
                trueOrFalse: false,
              },
              {
                content: `Infrastructure et code fragiles`,
                trueOrFalse: false,
              },
              {
                content: `Applications inefficaces ou dépassées `,
                trueOrFalse: false,
              },
              {
                content: `Toutes les options ci-dessus`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Que signifie la contraction DevOps ?",
          responses: {
            create: [
              {
                content: `Development And Operations`,
                trueOrFalse: true,
              },
              {
                content: `Digital and Operations`,
                trueOrFalse: false,
              },
              {
                content: `Drive and Operations`,
                trueOrFalse: false,
              },
              {
                content: `Aucune des propositions ci-dessus`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Parmi les suivants, quels sont les outils populaires en DevOps ?",
          responses: {
            create: [
              {
                content: `Jenkins & Monit`,
                trueOrFalse: false,
              },
              {
                content: `Nagios & ELK`,
                trueOrFalse: false,
              },
              {
                content: `Jenkins & Ansible`,
                trueOrFalse: false,
              },
              {
                content: `Toutes les options ci-dessus`,
                trueOrFalse: true,
              },
            ]
          }
        },
        {
          content: "Quelle plateforme cloud n'est-elle pas utilisée pour les implémentations DevOps ?",
          responses: {
            create: [
              {
                content: `Google Cloud`,
                trueOrFalse: false,
              },
              {
                content: `IBM`,
                trueOrFalse: true,
              },
              {
                content: `Microsoft Azure`,
                trueOrFalse: false,
              },
              {
                content: `Amazon Web Services`,
                trueOrFalse: false,
              },
            ]
          }
        },
        {
          content: "Parmi les langages suivants, quel est le langage de script populaire en DevOps ?",
          responses: {
            create: [
              {
                content: `Java`,
                trueOrFalse: false,
              },
              {
                content: `Python`,
                trueOrFalse: true,
              },
              {
                content: `Html`,
                trueOrFalse: false,
              },
              {
                content: `C++`,
                trueOrFalse: false,
              },
            ]
          }
        },
      ]
    }
  },
]