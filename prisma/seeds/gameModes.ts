import { Prisma } from "@prisma/client"

export const gameModes: Prisma.GameModeCreateInput[] = [
    {
      mode: 'duo'
    },
    {
      mode: 'carre'
    },
]