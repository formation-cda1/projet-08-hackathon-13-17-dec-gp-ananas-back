import { Prisma, PrismaClient } from '@prisma/client'
import cors from 'cors';

const prisma = new PrismaClient({
  log: [
    { level: 'query', emit: 'event' },
    { level: 'warn', emit: 'stdout' },
    { level: 'info', emit: 'stdout' },
    { level: 'error', emit: 'stdout' },
  ],
  errorFormat: 'pretty', // formatage des erreurs
  rejectOnNotFound: {
    findUnique: true, // rejet de requête quand unique non trouvé
  }
})

// souscription aux évènements
prisma.$on('query', (e) => {
  console.log('Query: ' + e.query)
  console.log('Duration: ' + e.duration)
})

const express = require('express')
const app = express()
const port = process.env.PORT || 4200
const bodyParser = require('body-parser')
const morgan = require('morgan')
const router = require('./routes/router')

require('dotenv').config()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors({
  origin: ["http://localhost:3000", "https://quizz-game.andriacapai.com"]
}))

// Logs des requêtes en mode développement
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(process.env.APP_BASE_URL, router)

const server = app.listen(port, () =>
  console.log(`
  🚀 Server ready at: http://localhost:${port}
  `)
)
