import { PrismaClient } from "@prisma/client"
const createError = require('http-errors')

class GetRankingsService {
  static async getRankings(cat: string, avg: boolean) {
    const prisma = new PrismaClient();
    let data: Object[] = [];

    let rankings = await prisma.score.findMany({
      orderBy: {
        [`${cat}Score`]: 'desc',
      }, 
      take: 5,
      include: {
        player: true,
      }
    })

    rankings.forEach(score => {
      let catScore;
      let catCount

      switch (cat) {
        case 'total': 
          catScore = avg ? score.totalScore / score.totalCount : score.totalScore;
          catCount = score.totalCount;
          break;
        case 'front':
          catScore = avg ? score.frontScore / score.frontCount : score.frontScore;
          catCount = score.frontCount;
          break;
        case 'back':
          catScore = avg ? score.backScore / score.backCount : score.backScore;
          catCount = score.backCount;
          break;
        case 'devOps':
          catScore = avg ? score.devOpsScore / score.devOpsCount : score.devOpsScore;
          catCount = score.devOpsCount;
          break;
        default: 
          console.log('Wrong category');
          break;
      }

      data.push({
        nickname: score.player.nickname,
        score: catScore,
        count: catCount,
      })
      
    });

    // remet les score dans l'order après le for of
    data.sort((a: any, b: any) => (a.score > b.score) ? -1 : 1)

    return data;
  }
}

module.exports = GetRankingsService;
