import { Request, Response, NextFunction } from "express"

const createError = require('http-errors')
const GetRankingsService = require('./getRankings.service')

class GetRankingsController {
  static getRankings = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let cat = req.params.category;
      let avg = req.params.avg === 'true' ? true : false;
      
      let rankings = await GetRankingsService.getRankings(cat, avg);

      res.status(200).json({
        status: true,
        message: "Rankings Controller !",
        data: rankings,
      });
    } catch (e: any) {
      next(createError(e.statusCode, e.message))
    }
  }
}

module.exports = GetRankingsController;

