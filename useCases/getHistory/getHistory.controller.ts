import { Request, Response, NextFunction } from "express"

const createError = require('http-errors')
const GetHistoryService = require('./getHistory.service')


class GetHistoryController {
  static getHistory = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let name = req.params.player;
      let history = await GetHistoryService.getHistory(name);

      res.status(200).json({
        status: true,
        message: "History Controller !",
        data: history
      });
    } catch (e: any) {
      next(createError(e.statusCode, e.message))
    }
  }
}

module.exports = GetHistoryController;