import { PrismaClient } from "@prisma/client";
// const createError = require('http-errors')

class GetHistoryService {
  static async getHistory(playerName: string) {
    const prisma = new PrismaClient();

    let player = await prisma.player.findUnique({
      where: {
        nickname: playerName.toLowerCase(),
      },
    })

    if (player) {
      let history = await prisma.game.findMany({
        where: {
          playerId: player.id
        },
        take: 10,
        orderBy: {
          createdAt: 'desc',
        },
        include: {
          category: true,
          histories: {
            orderBy: {
              position: 'asc',
            },
            include: {
              gameMode: true,
              question: true,
              response: true,
            }
          }
        }
      })

      if (history) {
        for (let game of history) {
          for (let question of game.histories) {
            if (!question.rightAnswer) {
              let answer = await prisma.question.findUnique({
                where: {
                  id: question.questionId
                },
                include: {
                  responses: {
                    where: {
                      trueOrFalse: true,
                    },
                    take: 1
                  }
                }
              })

              if (answer) {
                question.goodAnswerWas = answer.responses[0].content;
              }
            }
          }
        }

        return history ? history : false;
      }
    }
    return false;
  }
}

module.exports = GetHistoryService;