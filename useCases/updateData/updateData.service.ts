import { prisma, PrismaClient } from "@prisma/client"
import { Request } from "express"

// const createError = require('http-errors')
// require('dotenv').config()

class UpdateDataService {
  prisma = new PrismaClient();
  async updateData(req: Request) {
    let data: any = req.body

    // récupération du joueur
    let player = await this.prisma.player.findUnique({
      where: {
        nickname: data.player,
      },
    })
    
    // si le joueur n'existe pas, on le crée
    if (!player) {
      player = await this.prisma.player.create({
        data: {
          nickname: data.player
        }
      })
    }

    // ajout des scores et des positions sur chaque question
    for (let question of data.game.history) {
      question.score = question.rightAnswer ? 10 * question.gameModeId : 0;
      question.position = data.game.history.indexOf(question) + 1;
    };


    let game = await this.prisma.game.create({
      data: {
        score: data.game.history.map((a: any) => a.score).reduce(function (a: any, b: any) { return a + b }),
        playerId: player.id,
        categoryId: data.game.categoryId,
        histories: {
          create : data.game.history
        }
      }
    })

    if (game) await this.updateScore(player.id);
  }

  async updateScore(id: number) {
    let score = { totalScore: 0, backScore: 0, frontScore: 0, devOpsScore: 0, backCount: 0, frontCount: 0, devOpsCount: 0,totalCount: 0, playerId: id }

    const games = await this.prisma.game.findMany({
      where: {
        playerId: id
      }
    })

    if (games) {
      score.totalScore = games.map((a: any) => a.score).reduce(function (a: any, b: any) { return a + b })
      score.totalCount = games.length;

      games.forEach(game => {
        switch (game.categoryId) {
          case 1:
            score.frontScore += game.score;
            score.frontCount += 1;
            return;
          case 2:
            score.backScore += game.score;
            score.backCount += 1;
            return;
          case 3:
            score.devOpsScore += game.score;
            score.devOpsCount += 1;
            return;
        }
      });
    }

    let scoreExists = await this.prisma.score.findUnique({
      where: {
        playerId: id,
      }
    })

    if (scoreExists) {
      await this.prisma.score.update({
        where: {
          playerId: id,
        },
        data: score
      })

    } else {
      await this.prisma.score.create({
        data: score
      })
    }

  }
}

module.exports = UpdateDataService;