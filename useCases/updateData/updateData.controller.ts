import { Request, Response, NextFunction } from "express"
import { request } from "http"
const createError = require('http-errors')
const UpdateDataService = require('./updateData.service')

class UpdateDataController {
  static updateData = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const updateDataService = new UpdateDataService();
      let update = await updateDataService.updateData(req)

      res.status(200).json({
        status: true,
        message: "Update completed",
      });
    } catch (e: any) {
      next(createError(e.statusCode, e.message))
    }
  }
}

module.exports = UpdateDataController;