import { Request, Response, NextFunction } from "express"

const createError = require('http-errors')
const GetQuestionService = require('./getQuestions.service')

class GetQuestionController {
  static getQuestions = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let cat = req.params.category;
      let questions = await GetQuestionService.getQuestions(cat);

      res.status(200).json({
        status: true,
        message: "Question Controller !",
        data: questions,
      });
    } catch (e: any) {
      next(createError(e.statusCode, e.message))
    }
  }
}

module.exports = GetQuestionController;