import { PrismaClient } from "@prisma/client"

const createError = require('http-errors')
const utils = require('./../../utils/utils')
require('dotenv').config()

class GetQuestionService {
  static async getQuestions(cat: string) {
    const prisma = new PrismaClient();

    // pour obtenir des questions aléatoires, on établit un skip, un tri et un ordre aléatoires à chaque requête
    const skip = Math.max(0, Math.floor(Math.random() * 15) - 10);
    const orderBy = utils.randomPick(['id', 'content'])
    const orderDir = utils.randomPick(['asc', 'desc'])

    let category = await prisma.category.findUnique({
      where: {
        name: cat,
      },
    })

    if (category) {
      let questions = await prisma.question.findMany({
        where: {
          categoryId: category.id,
        },
        take: 10,
        skip: skip,
        orderBy: { [orderBy]: orderDir},
        include: {
          responses: true,
        }
      });

      return questions;
    } else {
      return false;
    }
  }
}

module.exports = GetQuestionService;