import { Request, Response, NextFunction } from "express"

const express = require('express')
const router = express.Router();
const createError = require('http-errors')
const GetQuestionController = require('../useCases/getQuestions/getQuestions.controller')
const GetHistoryController = require('../useCases/getHistory/getHistory.controller')
const GetRankingsController = require('../useCases/getRankings/getRankings.controller')
const UpdateDataController = require('../useCases/updateData/updateData.controller')

router.get('/questions/:category', GetQuestionController.getQuestions)

router.get('/history/:player', GetHistoryController.getHistory)

router.get('/rankings/:category/:avg', GetRankingsController.getRankings)

router.post('/update', UpdateDataController.updateData)

// gestion des erreurs et routes inexistantes
router.use(async (req: Request, res: Response, next: NextFunction) => {
  console.log(createError.NotFound('Route not found !'))
  next(createError.NotFound('Route not found !'))
})

router.use((err: any, req: Request, res: Response, next: NextFunction) => {
  res.status(err.status || 500).json({
      status: false,
      message: err.message
  })
})

module.exports = router;

